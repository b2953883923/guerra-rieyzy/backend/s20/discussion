// console.log("Hello World");

// [SECTION] Arithmetic Operators
	// +, -, *, /, %

	let numA = 5;
	let numB = 25;

	// Addition (+)
	let addition = numA + numB;
	console.log(addition);

	// Subtraction
	let minus = numB - numA;
	console.log(minus);

	// Multiplication
	let multiply = numA * numB;
	console.log(multiply);

	// Division
	let divide = numB / numA;
	console.log(divide);

	// Division and remainder.
	let modulo = numB % numA;
	console.log(modulo);



	// Assignment Operator
		//  Basic Assignment Operator (=)

		let assignedNumber = 8;
		console.log(assignedNumber);


	// Assignment and Arithmetic Operators

		// Addition Assignment Operator (+=)
			// The addition assignment operator adds the value of the right operand to a variable and assign the result of the addition operation to the variable.

		// long method
		// assignedNumber = assignedNumber +  2;
		// console.log("Result of the operation: " + assignedNumber);

		// short method
		assignedNumber += 2;
		console.log("Result of the addition assignment operator: " + assignedNumber);

		// Subtraction Assignment Operator
		assignedNumber -= 5;
		// assignedNumber = assignedNumber - 5; - long method.
		console.log("Result of the subtraction assignment operator: " + assignedNumber);

		// Multiplication Assignment Operator
		assignedNumber *= 4;
		// assignedNumber = assignedNumber * 4; - long method.
		console.log("Result of multiplication assignment operator: " + assignedNumber);


		// Division Assignment Operator
		assignedNumber /= 20;
		// assignedNumber = assignedNumber / 20;
		console.log("Result of division assignment operator: " + assignedNumber);

	// Increment and Decrement

		let z = 1;

		// The value of "z" is added by a value of one before returning the value and storing it in the variable "increment; This is called pre-increment"
		let increment = ++z;
		console.log("Result of pre-increment: " + increment);

		// The value of "z" was also increased even though we didn't implicitly specify any value reassignment
		console.log("Result of pre-increment: " + z);

		// The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one; post-increment.
		increment = z++;

		console.log("Result of post-increment: " + increment);

		// this will result to 3.
		console.log("Result of post-increment: " + z);

		// The value of "z" is drecreased by a value of one before returning and storing it in the "increment" variable; pre-decrement.
		let decrement = --z;

		console.log("Result of pre-decrement: " + decrement);

		console.log("Result of pre-decrement: " + z);
		// The value of is z is 2 after pre-decrement

		// The value of "z" is returned and stored into the "decrement" variable before we subtract a value of one from the value of "z".
		decrement = z--;
		console.log("Result of post-decrement: " + decrement);
		console.log("Result of post-decrement: " + z);


	// Type Coercion
		/*
			- Type coercion is the automatic or implicit conversion of values from one data type to another.
		*/

	let numC = "10";
	let numD = 12;

	// adding/concatenating a string and a number will result to a string.

	let coercion = numC + numD;
	console.log(coercion);
	// typeof - shows the data type of a value.
	console.log(typeof coercion);


	let numE = 16;
	let numF = 14;

	// This is our regular operation which result to a value with a number data type.
	let nonCoercion = numE + numF;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);



	// This will result to a number
	let numG = true + 1;
	console.log(numG);

	let numH = false + 1;
	console.log(numH);



	// Comparison Operators

	let juan = "juan";

	   // Equality Operator (==)

		// - Checks whether the operands are equal or have the same content.
		// Returns a boolean value.

		console.log(1 == 1);
		console.log(1 == 2);
		console.log(1 == '1');
		console.log(0 == false);

		console.log('juan' == 'juan');
		console.log('juan' == juan);


		// Inequality Operator (!=)
		/*
			- Checks whether the operands are not equal or have different contents.
			- The "!" means "not"
		*/


		console.log(1 != 1);
		console.log(1 != 2);
		console.log(1 != '1');
		console.log(0 != false);

		console.log('juan' != 'juan');
		console.log('juan' != juan);

		//  Strict Equality Operator
		/*
			- Checks whether the operands are equal or the same content

		*/
		console.log("")
		console.log(1 === 1);
		console.log(1 === 2);
		console.log(1 === '1');
		console.log(0 === false);

		console.log('juan' === 'juan');
		console.log('juan' === juan);

		// Stric Inequality Operator
		/*
			-check whether the operance are not equal or have different contents;
			-It still compares the data types of 2 values.
		*/
		console.log("")
		console.log(1 !== 1);
		console.log(1 !== 2);
		console.log(1 !== '1');
		console.log(0 !== false);

		console.log('juan' !== 'juan');
		console.log('juan' !== juan);

		// Relational Operators

			// Some comparison operators check whether one value is greater or less than to another value.

			let a = 50;
			let b = 65;

			// GT or greater than operator (>)
			let isGreaterThan = a > b;
			// LT is less than Operator (<)
			let isLessThan = a < b;
			// GTE Greater than or equal operator (>=)
			let isGte = a >= b;
			// LTE or less than or equal operator (<=)
			let isLte = a <= b;

			console.log("Result of GT, LT, GTE, LTE:")
			console.log(isGreaterThan)
			console.log(isLessThan)
			console.log(isGte)
			console.log(isLte)

			let numStr = "30";
			console.log(a > numStr);
			console.log(b <= numStr)

			let str = "thirty"
			console.log(b >= str)

			// logical operators

			let isLegalAge = true;
			let isRegistered = false;

			// Logical and Operator (&& double ampersand)
				// returns "true" if all operands/conditions are true
			let allRequirementsMet = isLegalAge && isRegistered;
			console.log("Result of logical AND Opertor:" + allRequirementsMet);

	// Logical or operator (|| - double pipe)
		// returns a true if at test least one operand is true.
		let someRequirementsMet = isLegalAge || isRegistered;
		console.log("Result of logical or Operator: " + someRequirementsMet)

	// Logical Not operator (! - exclamation point)
		// returns the opposite value.
	let someRequirementsNotMet = !isRegistered;
	console.log("Result of logical NOT Operator:" +  someRequirementsNotMet)